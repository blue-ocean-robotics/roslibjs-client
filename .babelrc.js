const { NODE_ENV } = process.env;
module.exports = {
    presets: [
        [
            '@babel/env',
            {
                exclude: [
                    'transform-async-to-generator',
                    'transform-regenerator'
                ],
                targets: {},
                loose: true,
                modules: false
            }
        ]
    ],
    plugins: ['@babel/proposal-object-rest-spread']
};
