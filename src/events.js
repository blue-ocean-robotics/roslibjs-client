export const CONNECTION = 'connection';
export const ERROR = 'error';
export const CLOSED = 'close';
export const DESTROYED = 'destroyed';
export const NEW_LISTENER = 'newListener';
