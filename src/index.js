import { CONNECTION, ERROR, CLOSED, DESTROYED, NEW_LISTENER } from './events';

import getRos from './rosProvider';
import { createCall, createPublish, createSubscribe } from './rosUtils';
import { customSetTimeout, withDeprecatedWarning } from './utils';

const defaultOptions = {
    url: 'ws://localhost:9090',
    reconnectInterval: 5000
};

const noop = () => {};
const instanceDestroyed = new Error('InstanceDestroyed');

function createRosClient(rosCreator, userOptions) {
    if (typeof rosCreator === 'object' && options == null) {
        userOptions = rosCreator;
        rosCreator = getRos;
    }

    const { reconnectInterval, url, ...options } = {
        ...defaultOptions,
        ...userOptions
    };

    const ros = rosCreator(options);
    let scheduled = false;
    let clearTimeout = null;

    const listeners = {
        [ERROR]: reconnectOnError,
        [CLOSED]: reconnectOnError,
        [NEW_LISTENER]: onNewListener
    };

    function destroy() {
        Object.keys(listeners).forEach(eventName => {
            ros.removeListener(eventName, listeners[eventName]);
        });

        ros.emit('distroy');
        if (typeof clearTimeout === 'function') {
            clearTimeout();
        }
        ros.close();
    }

    function onNewListener(eventName, listener) {
        if (eventName === CONNECTION && ros.isConnected === false) {
            if (ros.listenerCount(CONNECTION) === 1) {
                scheduleConnection();
            }
        }
    }

    function scheduleConnection(interval = 0) {
        if (scheduled) {
            return;
        }
        function connect() {
            clearTimeout = null;
            scheduled = false;
            ros.connect(url);
        }
        scheduled = true;
        clearTimeout = customSetTimeout(connect, interval);
    }

    function reconnectOnError() {
        ros.close();
        scheduleConnection(reconnectInterval);
    }

    function registerRosCallback(callback = noop) {
        if (ros.isConnected) {
            return callback(null, ros);
        }
        const onConnected = () => {
            ros.removeListener(DESTROYED, onDestroy);
            return callback(null, ros);
        };
        const onDestroy = (error = instanceDestroyed) => {
            ros.removeListener(CONNECTION, onConnected);
            return callback(error);
        };
        ros.once(CONNECTION, onConnected);
        ros.once(DESTROYED, onDestroy);
    }

    Object.keys(listeners).forEach(eventName => {
        ros.on(eventName, listeners[eventName]);
    });

    scheduleConnection();

    const api = {
        ros,
        registerRosCallback,
        destroy,
        subscribe: createSubscribe(registerRosCallback),
        publish: createPublish(registerRosCallback),
        callService: createCall(registerRosCallback)
    };
    api.service = {
        call: withDeprecatedWarning(
            api.callService,
            'topic.call is deprecated. Use .call directly instead',
            'callService'
        )
    };
    api.topic = {
        subscribe: withDeprecatedWarning(
            api.subscribe,
            'service.subscribe is deprecated. Use .subscribe directly instead',
            'subscribe'
        ),
        publish: withDeprecatedWarning(
            api.publish,
            'service.publish is deprecated. Use .publish directly instead',
            'publish'
        )
    };
    return api;
}

export { createRosClient };
