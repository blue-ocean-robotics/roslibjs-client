const { Service, ServiceRequest, Topic, Message, Ros } = require('roslib');

export function getService(options) {
    return new Service(options);
}

export function getRequest(options) {
    return new ServiceRequest(options);
}

export function getTopic(options) {
    return new Topic(options);
}

export function getMessage(options) {
    return new Message(options);
}

export function getRos(options) {
    const ros = new Ros(options);
    return ros;
}

export default getRos;
