import { getMessage, getRequest, getService, getTopic } from './rosProvider';

export function createCall(registerCallback) {
    return function (name, serviceType, payload) {
        return new Promise((resolve, reject) => {
            registerCallback((error, ros) => {
                if (error) {
                    reject(error);
                }
                const service = getService({
                    ros,
                    name,
                    serviceType
                });
                const request = getRequest(payload);
                service.callService(request, resolve, reject);
            });
        });
    };
}

export function createSubscribe(registerCallback) {
    let context = new WeakMap();
    const createDispose = key => () => {
        const disposeFn = context.get(key);
        if (typeof disposeFn === 'function') {
            disposeFn();
        }
    };
    return function (name, messageType, options = {}, handler) {
        if (typeof options === 'function') {
            if (typeof handler !== 'undefined') {
                return options(new Error('options should be an object'));
            }
            handler = options;
        }
        const key = {
            name,
            messageType,
            options,
            handler
        };

        registerCallback((error, ros) => {
            if (error) {
                handler(error);
            }
            const topic = getTopic({
                ros,
                name,
                messageType,
                ...options
            });
            topic.subscribe(handler);
            context.set(key, () => {
                return topic.unsubscribe(handler);
            });
        });
        return createDispose(key);
    };
}

export function createPublish(registerCallback) {
    return function (name, messageType, payload) {
        return new Promise((resolve, reject) => {
            registerCallback((error, ros) => {
                if (error) {
                    reject(error);
                }
                const topic = getTopic({
                    name,
                    ros,
                    messageType
                });

                topic.publish(getMessage(payload));
                resolve(topic.publish);
            });
        });
    };
}
