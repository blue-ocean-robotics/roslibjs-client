export function customSetTimeout(callback, timeout, ...args) {
    const timeId = setTimeout(callback, timeout, ...args);
    return () => clearTimeout(timeId);
}

export function withDeprecatedWarning(fn, message, warningKey) {
    const context = {};
    const warnOnce = function (message) {
        if (context.keysUsed && context.keysUsed.includes(warningKey)) {
            return;
        } else if (!Array.isArray(context.keysUsed)) {
            context.keysUsed = [];
        }
        context.keysUsed.push(warningKey);
        console.warn(message);
    };

    return (...args) => {
        warnOnce(message);
        return fn(...args);
    };
}
